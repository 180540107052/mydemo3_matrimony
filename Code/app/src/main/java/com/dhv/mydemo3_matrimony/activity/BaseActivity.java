package com.dhv.mydemo3_matrimony.activity;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.dhv.mydemo3_matrimony.R;

import butterknife.BindView;

public class BaseActivity extends AppCompatActivity {


        public void setUpActionBar(String title,boolean isBackArrow){
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
        }
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
