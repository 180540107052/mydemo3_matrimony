package com.dhv.mydemo3_matrimony.model;

import java.io.Serializable;

public class CityModel implements Serializable {
    int CityID;
    String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "CityModel{" +
                "CityID=" + CityID +
                ", Name='" + Name + '\'' +
                '}';
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }
}
