package com.dhv.mydemo3_matrimony.activity;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dhv.mydemo3_matrimony.R;
import com.dhv.mydemo3_matrimony.adapter.UserListAdapter;
import com.dhv.mydemo3_matrimony.database.TblUser;
import com.dhv.mydemo3_matrimony.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_user_list),true);
        setAdapter();
    }

    void setAdapter(){
        rcvUserList.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserID = new TblUser(UserListActivity.this).updateFavoriteStatus( 0, userList.get(position).getUserId());
                if(lastUpdatedUserID>0){
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemChanged(0,userList.size());
                }

            }

            @Override
            public void OnItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
    }
}
